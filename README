CAV Assignment 2

s0943942
Tsvetelina Petrova
================

Visualisation of human head


Compiling
---------

To compile the program run `make`.


Running
-------

To run the program use `./cav2`.


Structure
---------

Place all source files in `src`.
Place all header files in `include`.

These will ensure they are compiled by the Makefile.

Feel free to edit any of the provided files to add methods or functionality.


~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
1. Ray Casting

Forward ray casting was used for rendering because:

	1.1 Frontal view. The viewer is facing the skull, so want to see everything 
	up to the bone at the front of the head. If we wanted to see the hair maybe 
	at the back of the head, and the back of the skull, we would have used back 
	to front ray casting.
	1.2 Issues with back to front ray casting: will stop rendering at the back 
	of the skull and block everything else (similar high densities)
	
	2.1 CURRENT Alpha value. The opactity of a voxel is calculated via 
		*valToAlpha(value)* - line 125
		where value is the density value associated with the voxel, normalised 
		by 255 to shift range to [0, 1]. Assumtpion: the higher the density, the 
		smaller the opacity, i.e. a bone is non-transparent, the skin is. 
		Assume alpha for bone is ~1 and alpha for skin proportional to *value
	2.2 Accumulated Alpha:
			New alpha = Current Alpha + (1-Current Alpha) * Accumulated Alpha 
	Want to accumulate alpha because want to stop drawing when alpha ~1. In the 
	current code rendering is terminated when accumulated alpha is greater than 
	0.94
	2.3 Emitted light. Emitted light proportional to density:
		*emittedLight(value)* - line 141
		If *value* corresponds to a bone, the emitted light is white
		If *value* is skin, then the emitted light is proportional to value 
		but only in the Red channel to make the skin look red
		If *value* is neither bone nor skin then the voxel does not emit any 
		light.
	2.4 New accumulated intensity = Current Accumulated Intensity + 
						(1 - Accumulated) * Current Alpha * Emitted Light
						
	2.5 Render: if Accumulated Alpha <= .94
	
	
2. Interactive Control
	2.1 Skin transparency Control
		KEY 'p' (+): increases transparency by .025
		KEY 'm' (-): decreases transparency by .025
		
	Note: minimum transparency is -.25; maximum - 2.5.
	2.2 Rotation
		KEY 'q': X direction -> increase the angle by 10 degrees
		KEY 'w': X direction -> decrease the angle by 10 degrees
		KEY 'e': Y direction -> increase the angle by 10 degrees
		KEY 'r': Y direction -> decrease the angle by 10 degrees
		KEY 't': Z direction -> increase the angle by 10 degrees
		KEY 'y': Z direction -> decrease the angle by 10 degrees
		
3. Rendering
	The skull is drawn in 3D space in the order (x, y, z). The initial rotation 
	angles are shifted (i.e. are NOT 0, 0, 0) to make the model appear facing 
	the viewer.

~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
