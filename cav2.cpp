#include <stdio.h>
#include <stdlib.h>

#include <GL/glut.h>
#define GLUT_KEY_ESCAPE 27
#ifndef GLUT_WHEEL_UP
#define GLUT_WHEEL_UP 3
#define GLUT_WHEEL_DOWN 4
#endif

#include "Vector.h"
#include "Matrix.h"
#include "Volume.h"

#define WIDTH 512
#define HEIGHT 512

using namespace std;


float valToAlpha(float value);
Vector3 emittedLight(float value);

float transparency = .1;
float rotateX = 270.;
float rotateY = 20.;
float rotateZ = 250.;

static Volume* head = NULL;

void Update() {
	glutPostRedisplay();
}

void Draw() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glRotatef(rotateY, 0., 1., 0.);
	glRotatef(rotateX, 1., 0., 0.);
	glRotatef(rotateZ, 0., 0., 1.);
	

	const unsigned char threshBoneLow = 100;
	const unsigned char threshBoneHigh = 245;
	const unsigned char threshSkinLow = 65;
	const unsigned char threshSkinHigh = 135;
	Vector3 accIntensitySkin = Vector3(0., 0., 0.);
	Vector3 accIntensityBone = Vector3(0., 0., 0.);
	float alpha = 0.;
	float accAlphaSkin = 0.;
	float accAlphaBone = 0.;
	Vector3 skinLight = Vector3(1., 0., 0.);
	Vector3 boneLight = Vector3(1., 1., 1.);
	unsigned char val = 0;

	for(int y = 0; y < head->GetHeight(); y++)
		for(int z = 0; z < head->GetDepth(); z++) {

			accIntensitySkin = Vector3(0., 0., 0.);
			accIntensityBone = Vector3(0., 0., 0.);
			accAlphaSkin = 0.;
			accAlphaBone = 0.;
			for (int x = 0; x < head->GetWidth(); x++) {

				// val voxel density ====== scalar value

				// emitted light proportional to density
				// New alpha = Current Alpha + (1-Current Alpha) * Accumulated Alpha =====
				// Aacc = An + (1 - An)*Aacc

				//accAlpha = alpha + (1-alpha)*accAlpha;

				// New accumulated intensity = Current Intensity + (1 - New Alpha) * Current Alpha * Current Emitted Light
				// Iacc = Iacc + (1 - Aacc)*An*En      
				val = head->Get(x, y, z);

				if ( val >= threshSkinLow && val <= threshSkinHigh ) {	
		
					alpha = valToAlpha(val/255.);					
					skinLight = emittedLight(val/255.);
					
					accAlphaSkin = alpha + (1-alpha)*accAlphaSkin;
					accIntensitySkin += skinLight*(1-accAlphaSkin)*alpha;

        			glColor4f(accIntensitySkin[0], accIntensitySkin[1], accIntensitySkin[2], alpha);      
        			glBegin(GL_POINTS); 
        			glVertex3f(x, y, z);
        			glEnd();
        			
        			if (accAlphaSkin > .94) break;
				}
				
				if ( val >= threshBoneLow && val <= threshBoneHigh ) {	
				
		
					alpha = valToAlpha(val/255.);				
					boneLight = emittedLight(val/255.);
					
					accAlphaBone = alpha + (1-alpha)*accAlphaBone;
					accIntensityBone += boneLight*(1-accAlphaBone)*alpha;

        			glColor4f(accIntensityBone[0], accIntensityBone[1], accIntensityBone[2], alpha);
        			glBegin(GL_POINTS);  
        			glVertex3f(x, y, z);
        			glEnd();
        			
        			if (accAlphaBone > .94) break;
				}
				
				if (val <= threshSkinLow && val >= threshBoneHigh ) {
					glColor4f(0., 0., 0., 1.);
        			glBegin(GL_POINTS);  
        			glVertex3f(x, y, z);
        			glEnd();
				}
		}
	}

	glFlush();
	glutSwapBuffers();
	rotateX = 0.;
	rotateY = 0.;
	rotateZ = 0.;
	
}

float valToAlpha(float value) {

	// if skin transparency is low, show the bone
	if (transparency < 0.25) {
		// value > .5 means it's a bone
		if (value > 0.5) {
			//return 0.;
			return value - (transparency + 0.25f);
		}
	}
	
	// else hide the bone
	return value - transparency;

}

Vector3 emittedLight(float value) {
	Vector3 color = Vector3(0., 0., 0.);
	//bone
	if (value > 0.6) {
		color = Vector3(min(1.f, value + 0.2f), min(1.f, value + 0.2f), min(1.f, value + 0.2f));
	} 
	//skin
	else if (value >= 0.15) {
	color = Vector3(value, 0., 0.);
	} 
	//something
	else {
		color = Vector3(0., 0., 0.);
	}
	return color;
}

void KeyEvent(unsigned char key, int x, int y) {

	switch(key) {
	case GLUT_KEY_ESCAPE:
		exit(EXIT_SUCCESS);
		break;
	case 'p':
		if (transparency <= 2.5) {
			transparency += .025;
			printf("\nTransparency: %f", transparency);
		}
		break;
	case 'm':
		if ((transparency - .025) >= -.25) {
			transparency -= .025;
			printf("\nTransparency: %f", transparency);
		}
		break;
	// rotate Around X
	case 'q':
		if ( rotateX < .500) {
			rotateX += 10.;
		}
		break;
	case 'w':
		if (rotateX >= 0.) {
			rotateX -= 10.;
		}
		break;
		
	// rotate around Y
	case 'e':
		if ( rotateY < .500) {
			rotateY += 10.;
		}
		break;
	case 'r':
		if (rotateY >= 0.) {
			rotateY -= 10.;
		}
		break;
		
	// rotate around Z
	case 't':
		if ( rotateZ < .500) {
			rotateZ += 10.;
		}
		break;
	case 'y':
		if (rotateZ >= 0.) {
			rotateZ -= 10.;
		}
		break;
	}
}

int main(int argc, char **argv) {

	head = new Volume("head");


	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH|GLUT_MULTISAMPLE);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutCreateWindow("cav2");

	glClearColor(0., 0., 0., 0.);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, WIDTH, HEIGHT, 0, -512, 512);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);

	glutKeyboardFunc(KeyEvent);
	glutDisplayFunc(Draw);
	glutIdleFunc(Update);

	glutMainLoop();

	delete head;
};
